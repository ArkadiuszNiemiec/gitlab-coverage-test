#!/usr/bin/python3
# -*- coding: utf-8 -*-
import unittest


def plus(color, a, b):
    if color == 'red':
        return a + b
    if color == 'blue':
        return a + 2*b
    if color == 'green':
        return a + 3*b
    if color == 'brown':
        return a + 4*b


def minus(color, a, b):
    if color == 'red':
        return a - b
    if color == 'blue':
        return a - 2*b
    if color == 'green':
        return a - 3*b
    if color == 'brown':
        return a - 4*b


class TestValidate(unittest.TestCase):
    
    def test_plus(self):
        self.assertEqual(plus('red', 2, 3), 5)
        self.assertEqual(plus('blue', 2, 3), 8)

    def test_minus(self):
        self.assertEqual(minus('red', 2, 3), -1)


if __name__ == '__main__':
    unittest.main()
